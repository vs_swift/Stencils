//
//  Paper.swift
//  Stencils
//
//  Created by Private on 12/15/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Paper {
    
    var maxSymbols: Int
    var symbols: Int
    var content: String
    
    init(maxSymbols: Int = 4096) {
        self.symbols = 0
        self.maxSymbols = maxSymbols
        content = String()
    }
    func addContent(message: String) {
        let total = content.count + message.count
        if ( content.count == maxSymbols ) {
            print("Out Of Space")
        } else if ( total > maxSymbols ) {
            let last = maxSymbols - content.count
            if last < message.count {
                let newMassage = message.index(message.startIndex, offsetBy: last)
                content.append(String(message[..<newMassage]))
                symbols += content.count
            }
        }
        content.append(message)
        symbols += message.count
        print(content)
    }
}


