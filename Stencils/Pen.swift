//
//  Pen.swift
//  Stencils
//
//  Created by Private on 12/15/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Pen {
    
    var inkAmount: Int
    var inkCapacity: Int
    
    init (inkCapacity: Int = 1024) {
    self.inkAmount = inkCapacity
    self.inkCapacity = inkCapacity
    }
    
    func write(paper: Paper, message: String) {
        if ( inkAmount == 0 ) {
            print("Out Of Ink")
        } else if ( message.count > inkAmount ) {
            let end = message.index(message.startIndex, offsetBy: inkAmount)
            let newMessage = String(message[..<end])
            paper.addContent(message: newMessage)
            inkCapacity = 0
        } else {
            paper.addContent(message: message)
            inkAmount -= message.count
        }
    }
    
    func refill() {
        inkAmount = inkCapacity
    }
}
