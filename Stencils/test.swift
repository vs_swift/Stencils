//
//  test.swift
//  Stencils
//
//  Created by Private on 12/16/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation


/ Here the customer inputs a string

var someString = "Some Stringya"

// Here we set up the limit for characters

let maxCharacters = 12

// Here we count the characters

let charactersCount = someString.characters.count

// If count is less than maxCharacters, we print all the characters

if charactersCount <= maxCharacters {
    print(someString[someString.startIndex..<someString.endIndex])
    
}
    
    // And if the count is more than maxCharacters, we truncate every character a
    
else {
    
    // Here is the range that is over maxCharacters
    let rangeToTruncate = someString.index(someString.startIndex, offsetBy: maxCharacters)..<someString.endIndex
    
    // Here we remove the rangeToTruncate from the original someString
    
    someString.removeSubrange(rangeToTruncate)
    print(someString)
}
